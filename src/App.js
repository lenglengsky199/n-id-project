
import { Routes, Route } from "react-router-dom";
import Navigation from './layout/Navigation';
import Contact from './pages/frontend/Contact';
import Home from './pages/frontend/Home';
import Notfound from './layout/Notfound';
import MintNFT from "./pages/frontend/mint/MintNFT";
import { useWallet } from '@suiet/wallet-kit'
import isEmpty from "./utils";
import Name from "./pages/frontend/name/Name";

function App() {

    const {
        wallet,
        connected,
    } = useWallet();

    return (
        <>
            {/* The common navigation */}
            <Navigation />

            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/contact" element={<Contact />} />

                {/* Proteced route */}
                {(!isEmpty(wallet) || connected === true) && (
                    <Route path="/mint" element={<MintNFT />} />
                   
                    
                )}
                <Route path="/name" element={<Name />} />
                <Route path="*" element={<Notfound />} />
            </Routes>


            {/* The common footer */}
            {/* Here */}
        </>
    );
}

export default App;
