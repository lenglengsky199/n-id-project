import React from 'react'

export default function SafeArea(props) {
    return (
        <div className='mt-16 min-h-full'>
            {props.children}
        </div>
        
    )
}
