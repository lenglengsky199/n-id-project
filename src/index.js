import React from 'react';
import ReactDOM from 'react-dom/client';
import './input.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from 'react-router-dom';
import { getDefaultWallets, WalletProvider } from '@suiet/wallet-kit'
import "@suiet/wallet-kit/style.css";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <WalletProvider supportedWallets={getDefaultWallets()}>
        <BrowserRouter>
            <React.StrictMode>
                <App />
            </React.StrictMode>
        </BrowserRouter>
    </WalletProvider>
);
reportWebVitals();
