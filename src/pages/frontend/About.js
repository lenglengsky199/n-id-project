import React from 'react'
import { MdSecurityUpdateGood  } from 'react-icons/md';
import { AiOutlineSafetyCertificate  } from 'react-icons/ai';
import { VscSourceControl} from 'react-icons/vsc';
export default function About() {
    return (
        <>
          <div style={{backgroundImage: "linear-gradient(120deg, #fdfbfb 0%, #ebedee 100%)", height: '60vh'}}>
          <div className='container'>
                <h2 className='text-center'>Our Features</h2>
                <div className='row'>
                    <div className="col-4">
                        <div className="container mt-3">                          
                            <div className="card">
                            <div className="card-body text-center" style={{fontSize: "70px", color: "Brown"}}><MdSecurityUpdateGood/></div>
                            <h3 className='text-center'> Use Conveniently</h3>
                            <p>No worry when forget about long account number that have to copy and paste. Use NID instead</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="container mt-3">                          
                            <div className="card">
                            <div className="card-body text-center" style={{fontSize: "70px", color: "Brown"}}><AiOutlineSafetyCertificate /></div>
                            <h3 className='text-center'>Safe & Secure</h3>
                            <p>The underlying technology "blockchain" secure the info you put</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="container mt-3">                          
                            <div className="card">
                            <div className="card-body text-center" style={{fontSize: "60px", color: "Brown"}}><VscSourceControl/></div>
                            <h3 className='text-center'>YOur Full Control</h3>
                            <p>NFT-ID uses ERC-721 standard.Your NFT is securely stored with your own action and only you can manage it.</p>
                            </div>
                        </div>
                    </div>
                   
                </div>
                    
            </div>
          </div>
        </>
    )
}
