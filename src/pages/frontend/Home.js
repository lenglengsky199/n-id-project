import React from 'react'
import SafeArea from '../../components/SafeArea'
import ConnectWallet from './wallet/ConnectWallet'

export default function Home() {
    return (
        <SafeArea>
            <div className="mx-auto max-w-7xl py-6 sm:px-6 lg:px-8">
                <ConnectWallet />
            </div>
        </SafeArea>
    )
}
