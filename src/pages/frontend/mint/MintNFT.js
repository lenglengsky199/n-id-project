import React, { useState } from 'react'
import { useWallet } from '@suiet/wallet-kit'
import isEmpty from '../../../utils';
import SafeArea from '../../../components/SafeArea';

export default function MintNFT() {

    const {
        executeMoveCall,
    } = useWallet();

    const [nftName, setNftName] = useState()
    const [nftUrl, setNftUrl] = useState()
    const [nftDescription, setNftDescription] = useState()
    const [message, setMessage] = useState();
    const [loadImage, setloadImage] = useState(false);

    async function handleExecuteMoveCall() {

        if (isEmpty(nftName)) {
            setMessage({ text: 'No NFT name', color: 'red' });
            return;
        }
        if (isEmpty(nftUrl)) {
            setMessage({ text: 'No URL name', color: 'red' });
            return;
        }
        if (loadImage === true) {
            setMessage({ text: 'The image is invalid, cannot load.', color: 'red' });
            return;
        }
        if (isEmpty(nftDescription)) {
            setMessage({ text: 'No DESC name', color: 'red' });
            return;
        }

        try {
            const data = {
                packageObjectId: "0x2",
                module: "devnet_nft",
                function: "mint",
                typeArguments: [],
                arguments: [
                    nftName,
                    nftDescription,
                    nftUrl,
                ],
                gasBudget: 10000,
            }
            const resData = await executeMoveCall(data);
            console.log('executeMoveCall success', resData)
            setNftName('')
            setNftUrl('')
            setNftDescription('')
            setMessage({ text: 'executeMoveCall succeeded (see response in the console)', color: 'green' });
        } catch (e) {
            console.error('executeMoveCall failed', e)
            setMessage({ text: 'executeMoveCall failed (see response in the console)', color: 'red' });
        }
    }

    const onLoad = () => {
        console.log("loaded");
        setloadImage(false)
        // eslint-disable-next-line
    };

    return (

        <SafeArea>
            <div className="mx-auto max-w-7xl py-6 sm:px-6 lg:px-8">

                {!isEmpty(message) && (
                    <div onClick={() => setMessage(null)} className={(message.color === 'red' ? 'bg-red-500' : 'bg-green-500') + ' rounded-xl mb-3 py-2.5 px-3'}>
                        <p className='self-center'>{message.text}</p>
                    </div>
                )}

                <div className='bg-gray-200 w-full rounded-xl py-3'>
                    <div className='py-2'>
                        <input
                            onChange={(e) => setNftName(e.target.value)}
                            value={nftName}
                            name="name"
                            style={{ width: '-webkit-fill-available' }}
                            className='mx-3 rounded-lg px-2 py-2.5 bg-white'
                            placeholder='Name NFT'
                        />
                    </div>
                    <div className='py-2'>
                        <input
                            onChange={(e) => {
                                setNftUrl(e.target.value)
                                setloadImage(true)
                            }}
                            value={nftUrl}
                            name="url"
                            style={{ width: '-webkit-fill-available' }}
                            className='mx-3 rounded-lg px-2 py-2.5 bg-white'
                            placeholder='URL NFT'
                        />
                    </div>
                    <div className='py-2'>
                        <input
                            onChange={(e) => setNftDescription(e.target.value)}
                            value={nftDescription}
                            name="url"
                            style={{ width: '-webkit-fill-available' }}
                            className='mx-3 rounded-lg px-2 py-2.5 bg-white'
                            placeholder='URL Desc'
                        />
                    </div>
                    <div className='py-2'>
                        <button className="mx-3 bg-orange-800 text-white py-2 px-3 rounded-2xl" onClick={handleExecuteMoveCall}>Mint NFT</button>
                    </div>
                </div>

                {!isEmpty(nftUrl) && (
                    <center>
                        <div className='mt-2'>
                            {loadImage && (<p className='py-2 bg-red-300 px-3 rounded-lg'>Image loading ....</p>)}

                            {/* eslint-disable-next-line */}
                            <img
                                className='mt-3'
                                onLoad={onLoad}
                                src={nftUrl}
                            />
                        </div>
                    </center>
                )}

            </div>

        </SafeArea>
    )
}
