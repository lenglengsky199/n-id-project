import React, { useEffect, useState } from 'react'
import SafeArea from '../../../components/SafeArea';
import axios from 'axios';
export default function Name() {
    const [name, setName] = useState('');

    const getNameAPI = async()=>{
    console.log(name);
    await axios(`https://api-sui-names.herokuapp.com/getAddress?name=${name}`, {
        method: 'GET',
        headers: {
          'Access-Control-Allow-Origin': '*'
        },
        withCredentials: true,
      }).then(response => {
        console.log(response);
      })
      
    }

    useEffect (() =>{
    //    getNameAPI();
    },[])

    return (
        <>
            <SafeArea>
                <div className='bg-gray-900 width-full h-screen'>
                    <div className="mx-auto max-w-7xl py-6 sm:px-6 lg:px-8 ">
                        <h1 className='mt-10 p-10 text-center text-6xl text-orange-600'>Built the digital identity with NID</h1>
                        <p className='text-orange-600 text-center text-xl'>Get your .nid username that can be used to represent your identification</p>
                        <div className='pt-5 sm:pt-10 flex items-center col-span-12 col-start-1 sm:col-span-10 sm:col-start-2 md:col-span-6 md:col-start-4'>
                            <div className='relative w-full text-center'>
                                <input type="text" className="focus:outline-none placeholder:text-black bg-orange-400 border border-2 border-dotted border-orange-600 rounded-lg w-[50%] pl-10 p-2.5 text-2lg" placeholder="Search names"
                                    value={name}
                                    onChange={(e) => setName(e.target.value)}
                                    name='name' />
                                <button type='submit' className='p-2.5 ml-2 text-lg font-medium text-white bg-gradient-to-br from-orange-300 to-blue-300 rounded-lg hover:from-blue-300 hover:to-orange-800' onClick={() =>getNameAPI()}>Search</button>
                            </div>
                        </div>
                    </div>
                </div>
            </SafeArea>
        </>
    )
}
