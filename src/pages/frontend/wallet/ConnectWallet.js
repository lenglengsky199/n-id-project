import React, { useEffect, useState } from 'react'
import { useWallet } from '@suiet/wallet-kit'

export default function ConnectWallet() {

    const {
        wallet,
        connected,
        connecting,
        getAccounts,
    } = useWallet();

    const [accounts, setAccounts] = useState([])

    useEffect(() => {
        // If no connect or has wallet it should be not to get the account
        if (!connected || !wallet) return;

        const getAcc = async () => {
            const result = await getAccounts();
            setAccounts(result);
        }
        // Get the account if owner
        getAcc();
        // eslint-disable-next-line
    }, [connected, wallet])

    return (
        <>
            {!connected ? (
                <p>
                    Connect DApp with Suiet wallet from now!
                </p>
            ) : (
                <div className='bg-orange-800 py-3 px-3 rounded-xl text-white'>
                    <div>
                        <p>Current wallet: {wallet ? wallet.adapter.name : "null"}</p>
                        <p>
                            wallet status:{" "}
                            {connecting
                                ? "connecting"
                                : connected
                                    ? "connected"
                                    : "disconnected"}
                        </p>
                        <p>wallet accounts: {JSON.stringify(accounts)}</p>
                    </div>
                </div>
            )}
        </>
    )
}
